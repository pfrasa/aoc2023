module PipeMaze (challengePair) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.IntMap (IntMap, (!))
import qualified Data.IntMap as IM
import Data.List (find)
import Data.Maybe (mapMaybe, fromJust)
import Data.Set (Set)
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Data.Tuple.Extra (second)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve1' testData' challengeData'

data Tile
  = NorthSouth
  | EastWest
  | NorthEast
  | NorthWest
  | SouthWest
  | SouthEast
  | Ground
  | Start
  deriving (Eq, Show)

type Grid = IntMap (IntMap Tile)

(!!!) :: IntMap (IntMap a) -> (Int, Int) -> a
(!!!) m (i, j) = (m ! i) ! j

rows, cols :: Grid -> Int
rows = IM.size
cols = IM.size . (! 1)

solve1' :: ByteString -> String
solve1' = show . (`div` 2) . findMaxLoop . readInput
  where
    findMaxLoop :: Grid -> Int
    findMaxLoop grid =
      let start = findStart grid
      in  head $ mapMaybe (findLoopFrom grid start) (neighbors start)
    
    findStart :: Grid -> (Int, Int)
    findStart grid = fromJust $ find (\c -> grid !!! c == Start) $ [(x, y) | x <- [1..rows grid], y <- [1.. cols grid]]

    findLoopFrom :: Grid -> (Int, Int) -> (Int, Int) -> Maybe Int
    findLoopFrom grid start next@(nextX, nextY) =
      findLoop grid (nextX, nextY, S.fromList [start, next], 2)

type Acc = (Int, Int, Set (Int, Int), Int)
findLoop :: Grid -> Acc -> Maybe Int
findLoop grid = go
  where
    go :: Acc -> Maybe Int
    go (x, y, visited, steps) =
      case filter (connected grid (x,y)) (filter validRange (neighbors (x, y))) of
        [v, w] ->
          if v `S.member` visited && w `S.member` visited
            then Just steps
            else
              let newTile@(newX, newY) = if v `S.member` visited then w else v
              in  go (newX, newY, S.insert newTile visited, steps + 1)
        _ -> Nothing

    validRange :: (Int, Int) -> Bool
    validRange (x, y) = x >= 1 && x <= rows grid && y >= 1 && y <= cols grid

neighbors :: (Int, Int) -> [(Int, Int)]
neighbors (x, y) = [(x-1, y), (x, y+1), (x+1, y), (x, y-1)]

connected :: Grid -> (Int, Int) -> (Int, Int) -> Bool
connected grid (i, j) (k, l)
  | i-1 == k && j == l =
      tile1 `elem` [NorthSouth, NorthEast, NorthWest, Start] &&
      tile2 `elem` [NorthSouth, SouthEast, SouthWest, Start]
  | i == k && j+1 == l =
      tile1 `elem` [EastWest, NorthEast, SouthEast, Start] &&
      tile2 `elem` [EastWest, NorthWest, SouthWest, Start]
  | i+1 == k && j == l =
      tile1 `elem` [NorthSouth, SouthWest, SouthEast, Start] &&
      tile2 `elem` [NorthSouth, NorthEast, NorthWest, Start]
  | i == k && j-1 == l =
      tile1 `elem` [EastWest, NorthWest, SouthWest, Start] &&
      tile2 `elem` [EastWest, NorthEast, SouthEast, Start]
  | otherwise = False
  where
    tile1 = grid !!! (i, j)
    tile2 = grid !!! (k, l)

readInput :: ByteString -> Grid
readInput = toMap . ((parseChar <$>) . T.unpack <$>) . T.lines . decodeUtf8
  where
    toMap :: [[Tile]] -> Grid
    toMap = IM.fromList . (second (IM.fromList . zip [1..]) <$>) . zip [1..]

parseChar :: Char -> Tile
parseChar '|' = NorthSouth
parseChar '-' = EastWest
parseChar 'L' = NorthEast
parseChar 'J' = NorthWest
parseChar '7' = SouthWest
parseChar 'F' = SouthEast
parseChar '.' = Ground
parseChar 'S' = Start
parseChar c = error ("invalid char " ++ [c])

testData' :: ByteString
testData' = $(embedFile "data/test/10-Maze-1.txt")

testData2' :: ByteString
testData2' = $(embedFile "data/test/10-Maze-2.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/10-Maze.txt")
